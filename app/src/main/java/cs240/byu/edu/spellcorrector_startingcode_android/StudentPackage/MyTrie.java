package cs240.byu.edu.spellcorrector_startingcode_android.StudentPackage;

import java.util.TreeSet;

/**
 * Created by melanie on 12/05/2016.
 */
public class MyTrie implements ITrie {
    private MyNode root;
    private int nodeCount;
    private TreeSet<String> myTree;

    public MyTrie () {
        root = new MyNode();
        nodeCount = 1;
        myTree = new TreeSet<>();
    }

    @Override
    public void add(String word) {
        MyNode ptr = root;
        word = word.toLowerCase();

        for (int i = 0; i < word.length(); i++) {
            int index = word.charAt(i) - 'a';
            if (ptr.nodeFound(index) == null) {
                // create a new node
                ptr.addNode(index);
                nodeCount++;
            }
            ptr = ptr.nodeFound(index);
        }
        ptr.updateCount();
        myTree.add(word);

    }

    @Override
    public INode find(String word) {
        MyNode ptr = root;
        word = word.toLowerCase();

        for (int i = 0; i < word.length(); i++) {
            int index = word.charAt(i) - 'a';
            if (ptr.nodeFound(index) == null) {
                return null;
            }
            ptr = ptr.nodeFound(index);
        }
        if (ptr.getValue() == 0) {
            return null;
        }
        return ptr;
    }

    @Override
    public int getWordCount() {
        return myTree.size();
    }

    @Override
    public int getNodeCount() {
        return nodeCount;
    }
}
