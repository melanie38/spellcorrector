package cs240.byu.edu.spellcorrector_startingcode_android.StudentPackage;

/**
 * Created by melanie on 12/05/2016.
 */
public class MyNode implements ITrie.INode {
    private int count;
    private MyNode[] nodes;

    public MyNode() {
        count = 0;
        nodes = new MyNode[26];
    }
    public MyNode nodeFound(int index) {
        return nodes[index];
    }
    public void addNode(int index) {
        nodes[index] = new MyNode();
    }
    public void updateCount() {
        count++;
    }

    @Override
    public int getValue() {
        return count;
    }
}
