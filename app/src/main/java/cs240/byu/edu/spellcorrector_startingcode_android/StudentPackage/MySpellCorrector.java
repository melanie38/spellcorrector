package cs240.byu.edu.spellcorrector_startingcode_android.StudentPackage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TreeSet;

/**
 * Created by melanie on 12/05/2016.
 */
public class MySpellCorrector implements ISpellCorrector {
    private MyTrie myTrie;
    private TreeSet<String> found;
    private TreeSet<String> notFound;

    public MySpellCorrector() {
        myTrie = new MyTrie();
        found = new TreeSet<>();
        notFound = new TreeSet<>();
    }

    @Override
    public void useDictionary(InputStreamReader dictionaryFile) throws IOException {
        BufferedReader toRead = new BufferedReader(dictionaryFile);
        String word = toRead.readLine();

        while (word != null) {
            myTrie.add(word);
            word = toRead.readLine();
        }
    }

    @Override
    public String suggestSimilarWord(String inputWord) throws NoSimilarWordFoundException {
        if (myTrie.find(inputWord) == null) {
            StringBuilder word = new StringBuilder(inputWord);

            deletion(word);
            transposition(word);
            alteration(word);
            insertion(word);
        }
        return null;
    }
    public void deletion(StringBuilder word) {
        StringBuilder toMod;
        for (int i = 0; i < word.length(); i++) {
            toMod = word;
            toMod.deleteCharAt(i);
            isFound(toMod.toString());
        }
    }
    public void transposition(StringBuilder word) {
        StringBuilder toMod;
        for (int i = 0; i < word.length() - 1; i ++) {
            toMod = word;
            char a = word.charAt(i);
            char b = word.charAt(i+1);
            toMod.setCharAt(i, b);
            toMod.setCharAt(i+1, a);
            isFound(toMod.toString());
        }
    }
    public void alteration(StringBuilder word) {
        StringBuilder toMod;
        for (int i = 0; i < word.length(); i++) {
            char[] alpha = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
            toMod = word;
            for (int j = 0; j < alpha.length; j++) {
                toMod.setCharAt(i, alpha[j]);
                isFound(toMod.toString());
            }
        }
    }
    public void insertion(StringBuilder word) {
        StringBuilder toMod;
        for (int i = 0; i < word.length() + 1; i++) {
            char[] alpha = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
            toMod = word;
            for (int j = 0; j < alpha.length; j++) {
                toMod.insert(i, alpha[j]);
                isFound(toMod.toString());
            }
        }

    }
    public void isFound(String word) {
        if (myTrie.find(word) != null) {
            found.add(word);
        }
        else {
            notFound.add(word);
        }
    }
}
